﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CreditsController : MonoBehaviour {

    public AudioSource SFXSound;

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            GoToMenu();
        }
    }

    public void GoToMenu() {
        SFXSound.Play();
        StartCoroutine(waitToMenu());
    }

    IEnumerator waitToMenu() {
        yield return new WaitForSeconds(0.3f);

        SceneManager.LoadScene("Menu");
        SceneManager.UnloadScene("Credits");
    }
}
