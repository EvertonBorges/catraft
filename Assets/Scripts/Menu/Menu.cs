﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Menu : MonoBehaviour {

    public Image panelExitApplication;
    public AudioSource SFXClick;

    void Start() {
        panelExitApplication.gameObject.SetActive(false);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            ExitButton();
        }
    }

	void LoadMenu(){
		SceneManager.LoadScene ("Menu");
		SceneManager.UnloadScene ("Splash - Ocean");
	}

    public void StartButton() {
        SFXClick.Play();
        StartCoroutine(waitToStart());
    }

    public void CreditButton() {
        SFXClick.Play();
        StartCoroutine(waitToCredit());
    }

    public void ExitButton() {
        SFXClick.Play();
        panelExitApplication.gameObject.SetActive(true);
    }

    public void ConfirmExitButton() {
        Application.Quit();
    }

    public void CancelExitButton() {
        panelExitApplication.gameObject.SetActive(false);
    }

    IEnumerator waitToStart() {
        yield return new WaitForSeconds(0.3f);

        SceneManager.LoadScene("Game");
        SceneManager.UnloadScene("Menu");
    }

    IEnumerator waitToCredit() {
        yield return new WaitForSeconds(0.3f);

        SceneManager.LoadScene("Credits");
        SceneManager.UnloadScene("Menu");
    }
}
