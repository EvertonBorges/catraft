﻿using UnityEngine;
using System.Collections;

public class ScrollTextureChilds : MonoBehaviour {

	public float velocityX, velocityY;

	[Header("Sort Offset")]
	public float timeToSortNumberAgain;
	public bool isSort;
	public float minX, maxX;
	public float minY, maxY;

	private float m_currentOffsetX, m_currentOffsetY;
	private float m_currentTime;
    private bool isPaused;
    private float startVelocityY;

	// Use this for initialization
	void Start () {
		m_currentTime = 0f;
        startVelocityY = velocityY;
        isPaused = false;
	}

	// Update is called once per frame
	void Update () {

		if (isPaused) {
			return;
		}

		m_currentOffsetX += velocityX;
		if (m_currentOffsetX > 1.0f) {
			m_currentOffsetX -= 1f;
		}

		m_currentOffsetY += velocityY;
		if (m_currentOffsetY > 1f) {
			m_currentOffsetY -= 1f;
		}

		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).GetComponent<Renderer> ().material.mainTextureOffset = new Vector2 (m_currentOffsetX, 
																									   m_currentOffsetY);
		}

		m_currentTime += Time.deltaTime;

		if (!(m_currentTime > timeToSortNumberAgain)) {
			return;
		} else{
			m_currentTime = 0f;

			if (isSort) {
				velocityX = Random.Range (minX, maxX);
				velocityY = Random.Range (minY, maxY);
			}
		}
	}

	void SetPaused(bool isPaused) {
        this.isPaused = isPaused;
	}

    void SetVelocity(float velocity) {
        this.velocityY = startVelocityY * velocity;
    }
}
