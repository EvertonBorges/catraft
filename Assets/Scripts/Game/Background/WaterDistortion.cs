﻿using UnityEngine;
using System.Collections;

public class WaterDistortion : MonoBehaviour {

	[Range(-50f,0f)]
	public float minDistortion;
	[Range(0f,50f)]
	public float maxDistortion;

    // Time Controllers
    [Range(0f,0.1f)] public float minTimeInterval;
    [Range(0.1f,0.5f)] public float maxTimeInterval;
    [Range(0.25f,0.5f)] public float minTimeToDistortion;
    [Range(0.5f,1f)] public float maxTimeToDistortion;
    private float timeInterval;
    private float timeToDistortion;
    private float actualTime;
    private float distortionX;
    private float distortionY;
    private bool isDistortion;
    private bool isInterval;

	// Use this for initialization
	void Start () {
		actualTime = 0f;
        distortionX = 0f;
        distortionY = 0f;
        timeInterval = Random.Range(minTimeInterval, maxTimeInterval);
        timeToDistortion = Random.Range(minTimeToDistortion, maxTimeToDistortion);
        isDistortion = true;
        isInterval = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (actualTime < timeInterval && isInterval) {
			actualTime += Time.deltaTime;
			if (actualTime >= timeInterval) {
				actualTime = 0f;
                distortionX = 0f;
                distortionY = 0f;
                timeInterval = Random.Range(minTimeInterval, maxTimeInterval);
                isInterval = false;
                isDistortion = true;
			}
		}

		if (actualTime < timeToDistortion && isDistortion) {
            actualTime += Time.deltaTime;

            if (distortionX == 0f && distortionY == 0f) {
                distortionX = Random.Range(minDistortion, maxDistortion) * Time.deltaTime;
                distortionY = Random.Range(minDistortion, maxDistortion) * Time.deltaTime * 1.25f;
            }
            Vector2 localScale = transform.localScale;
            Vector2 distortion = new Vector2(distortionX, distortionY);
            localScale += distortion;
            if (localScale.x < 6f) {
                localScale.x = 6f;
            } else if (localScale.x > 100f) {
                localScale.x = 100f;
            }
            if (localScale.y < 12f) {
                localScale.y = 12f;
            } else if (localScale.y > 200f) {
                localScale.y = 200f;
            }
            transform.localScale = localScale;

            if (actualTime > timeToDistortion) {
                actualTime = 0f;
                timeToDistortion = Random.Range(minTimeToDistortion, maxTimeToDistortion);
                isInterval = true;
                isDistortion = false;
            }
		}

        
    }
}