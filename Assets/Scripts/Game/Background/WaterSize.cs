﻿using UnityEngine;
using System.Collections;

public class WaterSize : MonoBehaviour {

    public float minY, maxY;

    void Start() {
        minY = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height)).y;
        maxY = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, 0f)).y;
    }

    void Update() {
        float minY = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height)).y;
        float maxY = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, 0f)).y;

        if (this.minY != minY || this.maxY != maxY) {
            this.minY = minY;
            this.maxY = maxY;
        }
    }
}
