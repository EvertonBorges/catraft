﻿using UnityEngine;
using System.Collections;

public class FullScreen : MonoBehaviour {

	private float halfScreenSizeX;
	private float halfScreenSizeY;

	// Use this for initialization
	void Start () {
		halfScreenSizeX = Screen.width * 1.5f;
		halfScreenSizeY = Screen.height * 1.5f;

		Vector2 positionHalf = new Vector2 (halfScreenSizeX, halfScreenSizeY);

		Vector2 fullScale = Camera.main.ScreenToWorldPoint(positionHalf);

		transform.localScale = new Vector3 (fullScale.x, fullScale.y, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		float halfScreenSizeX = Screen.width * 1.5f;
		float halfScreenSizeY = Screen.height * 1.5f;

		if (this.halfScreenSizeX != halfScreenSizeX || this.halfScreenSizeY != halfScreenSizeY) {
			Vector2 positionHalf = new Vector2 (halfScreenSizeX, halfScreenSizeY);

			Vector2 fullScale = Camera.main.ScreenToWorldPoint(positionHalf);

			transform.localScale = new Vector3 (fullScale.x, fullScale.y, 1f);

			this.halfScreenSizeX = halfScreenSizeX;
			this.halfScreenSizeY = halfScreenSizeY;
		}
	}
}