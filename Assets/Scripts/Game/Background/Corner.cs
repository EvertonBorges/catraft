﻿using UnityEngine;
using System.Collections;

public class Corner : MonoBehaviour {

	public bool isLeft;
	private Vector3 viewportSize;
	private Vector3 position;

	// Use this for initialization
	void Start () {
		Vector3 positionLeft = Camera.main.ViewportToWorldPoint (new Vector3 (0.0625f, 0.5f));
		Vector3 positionRight = Camera.main.ViewportToWorldPoint (new Vector3 (0.9375f, 0.5f));

		viewportSize = Camera.main.ViewportToScreenPoint (new Vector3(0.525f, 0.6f, 0f));

		if (isLeft) {
			position = positionLeft;
		} else {
			position = positionRight;
            viewportSize = new Vector3(-viewportSize.x, viewportSize.y, viewportSize.z);
		}

        position = new Vector3(position.x, position.y, 0f);
        viewportSize = new Vector3(viewportSize.x, viewportSize.y, 1f);

		transform.position = position;
		transform.localScale = viewportSize;
	}

	// Update is called once per frame
	void Update () {
		Vector3 positionLeft = Camera.main.ViewportToWorldPoint (new Vector3 (0.0625f, 0.5f));
		Vector3 positionRight = Camera.main.ViewportToWorldPoint (new Vector3 (0.9375f, 0.5f));

		Vector3 viewportSize = Camera.main.ViewportToWorldPoint (new Vector3(0.525f, 0.6f, 0f));

		if ((position != positionLeft && position != positionRight) || (this.viewportSize != viewportSize)) {
			if (isLeft) {
				position = positionLeft;
			} else {
				position = positionRight;
                viewportSize = new Vector3(-viewportSize.x, viewportSize.y, viewportSize.z);
            }

            position = new Vector3(position.x, position.y, 0f);
            viewportSize = new Vector3(viewportSize.x, viewportSize.y, 1f);

            transform.position = position;
			transform.localScale = viewportSize;
		}
	}
}
