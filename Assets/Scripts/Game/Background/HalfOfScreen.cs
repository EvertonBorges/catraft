﻿using UnityEngine;
using System.Collections;

public class HalfOfScreen : MonoBehaviour {

    public bool isLeft;

	// Use this for initialization
	void Start () {
        transform.localScale = Camera.main.ViewportToWorldPoint(new Vector3(0.25f, 0.5f, 1f));
        if (isLeft) {
            transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.25f, 0.5f, 10f));
        } else {
            transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.75f, 0.5f, 10f));
        }
	}
	
	// Update is called once per frame
	void Update () {
	    Vector3 newScale = Camera.main.ViewportToWorldPoint(new Vector3(0.25f, 0.5f, 0.5f));
        if (transform.localScale != newScale) {
            transform.localScale = newScale;
            if (isLeft) {
                transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.25f, 0.5f, 10f));
            } else {
                transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.75f, 0.5f, 10f));
            }
        }
    }
}
