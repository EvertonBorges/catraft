﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public partial class Message{
	public const string BestScore = "BestScore";
}

public class GameManager : MonoBehaviour {

    public GameObject player;
    public GameObject obstacleManager;
    public AudioSource ambienceAudio;
	public float speedGame;

    [Header("Text Start Game")]
    public Text textStart;

	[Header("Texts")]
	public Text textDistance;
	public Image[] textLife;

	[Header("Tutorial Panel")]
	public Image tutorialPanel;

    [Header("Pause Panel")]
    public Image pausePanel;
    public Text BestScorePausePanel;

    [Header("GameOver Panel")]
    public Image gameoverPanel;
    public Text BestScoreGameOverPanel;
    public Text ActualScoreGameOverPanel;

    private float actualTime = 0f;
	private float globalTime = 0f;
	public int level = 0;
	private float startedSpeedGame;
	private float distance;
    private bool isPaused = false;
    private bool setPausedTrigger = false;
    private bool gameIsStarting = false;
    private bool gameIsStarted = false;
    private bool setGameOverTrigger = false;
    private bool isGameOver = false;
    private bool listening = false;
	private bool isTutorial = false;

    private Animator m_AnimatorPausePanel;
    private Animator m_AnimatorGameOverPanel;

	// Use this for initialization
	void Start () {
		startedSpeedGame = speedGame;
        m_AnimatorPausePanel = pausePanel.GetComponent<Animator>();
        m_AnimatorGameOverPanel = gameoverPanel.GetComponent<Animator>();

        player.GetComponent<PlayerController>().enabled = false;
        obstacleManager.SetActive(false);
		tutorialPanel.gameObject.SetActive (false);

        StartCoroutine(startListening());
    }

    // Update is called once per frame
    void Update() {

        if (isGameOver) {
            UpdateAnimatorParameters();
            return;
        }

        if(!listening) {
            return;
        }

		if (listening && gameIsStarted && !isTutorial) {
			isTutorial = true;
			tutorialPanel.gameObject.SetActive (true);
		}


        if (!gameIsStarting && listening) {
            if (Input.GetMouseButtonDown(0)) {
                player.GetComponent<Rigidbody2D>().velocity = Vector2.down * speedGame;
                textStart.enabled = false;
                gameIsStarting = true;
                ambienceAudio.Play();
            }

            if (Input.touchCount > 0) {
                TouchPhase press = Input.GetTouch(0).phase;
                bool isTouch = (press == TouchPhase.Began || press == TouchPhase.Moved || press == TouchPhase.Stationary || press == TouchPhase.Ended || press == TouchPhase.Canceled || Input.GetMouseButtonDown(0));
                if (isTouch) {
                    player.GetComponent<Rigidbody2D>().velocity = Vector2.down * speedGame;
                    textStart.enabled = false;
                    gameIsStarting = true;
                    ambienceAudio.Play();
                }
            }
            return;
        }

        if (!gameIsStarted && listening && gameIsStarting) {
            UpdateAnimatorParameters();
            StartGame();
            return;
        }

        if (isPaused) {
            UpdateAnimatorParameters();
            CheckPause();
            return;
        }

        if (!m_AnimatorPausePanel.IsInTransition(0)) {
            UpdateAnimatorParameters();
            globalTime += Time.deltaTime;
            CheckPause();
            ChangeSpeedGame();
            ChangeDistance();
        }
	}

    void UpdateAnimatorParameters() {

        m_AnimatorPausePanel.SetBool("IsPaused", this.isPaused);

        // Pause Menu
        if (setPausedTrigger) {
            m_AnimatorPausePanel.SetTrigger("PauseMenu");
            setPausedTrigger = false;
        } else {
            m_AnimatorPausePanel.ResetTrigger("PauseMenu");
        }

        // Game Over Menu
        if (setGameOverTrigger) {
            m_AnimatorGameOverPanel.SetTrigger("GameOver");
            setGameOverTrigger = false;
        } else {
            m_AnimatorGameOverPanel.ResetTrigger("GameOver");
        }
    }

	void ChangeSpeedGame() {
		actualTime += Time.deltaTime;
		
		if (actualTime > 10f && level < 10) {
			actualTime = 0f;
			level++;
			speedGame *= startedSpeedGame;
			GameObject.Find ("Corners").SendMessage ("SetVelocity", speedGame);
            obstacleManager.SendMessage("SetLevel");
			GameObject.Find ("Water").GetComponent<Animator> ().speed *= 1.1f;
			GameObject.Find ("Player").GetComponent<PlayerController> ().force *= 1.1f;
		}
	}

	void ChangeDistance() {
		this.distance = globalTime * speedGame;
		int distance = (int)this.distance;
		textDistance.text = string.Format ("{0}m", distance);
	}

	void ChangeLife(int life) {
		if (life > 0) {
			textLife [life].color = new Color (255, 255, 255, 0);
		} else {
            isGameOver = true;
            GameOver();
		}
	}


    void CheckPause() {
        bool isPaused = this.isPaused;

        // Descubro se pausou o jogo.
        if (Input.GetKeyDown(KeyCode.Escape)) {
            this.isPaused = !this.isPaused;
        }

        // Condição ativada apenas quando o pause muda de estado.
        if (isPaused != this.isPaused) {
            ChangePause();
        }
    }

    // Tira jogo do pause
    public void ResumeGame() {
        this.isPaused = false;

        ChangePause();
    }

    void StartGame() {
        if (player.transform.position.y < Camera.main.ViewportToWorldPoint(new Vector3(0f, 0.75f, 0f)).y) {
            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            player.GetComponent<PlayerController>().enabled = true;
            player.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.75f, 9f));
            gameIsStarted = true;
            obstacleManager.SetActive(true);
        }
    }

    IEnumerator startListening() {
        yield return new WaitForSeconds(1f);

        player.transform.position = new Vector3(0f, 6f, -1f);
        listening = true;
    }

	public void GameOver(){
        SaveBestScore();
        ChangeGameOver();
	}

    public void GoToMenu() {
        SaveBestScore();
		SceneManager.LoadScene("Menu");
        SceneManager.UnloadScene("Game");
    }

    public void PauseTheGame() {
        if (!this.isPaused && listening && !isGameOver) {
            this.isPaused = true;
            ChangePause();
        }
    }

    public void RestartTheGame() {
		SceneManager.LoadScene("Game");
		SceneManager.UnloadScene("Game");
    }

    //Método comum que alterna o pause do jogo.
    void ChangePause() {
        player.SendMessage("SetPaused", this.isPaused); // Pausar o player
        obstacleManager.SendMessage("SetPaused", this.isPaused); // Pausar a contrução de novos obstáculos
        GameObject.Find("Corners").gameObject.SendMessage("SetPaused", this.isPaused); // Pausar o offset dos corners

        // Pausar os obstaculos
        GameObject[] obstaclesInScene = GameObject.FindGameObjectsWithTag("Obstacle");
        for (int i = 0; i < obstaclesInScene.Length; i++) {
            obstaclesInScene[i].SendMessage("SetMove", this.isPaused);
        }

        // Pausar os monstros
        GameObject[] monstersInScene = GameObject.FindGameObjectsWithTag("Monster");
        for (int i = 0; i < monstersInScene.Length; i++) {
            monstersInScene[i].SendMessage("SetMove", this.isPaused);
        }

        // Quando o pause é ativado, parar todas as animações e mostrar o menu de pause.
        if (this.isPaused) {
            BestScorePausePanel.text = string.Format("  {0}m", GetBestScore());

            setPausedTrigger = true;
            GameObject.Find("Water").GetComponent<Animator>().speed = 0f;
            for (int i = 0; i < obstaclesInScene.Length; i++) {
                obstaclesInScene[i].transform.GetChild(0).GetComponent<Animator>().speed = 0f;
            }
            for (int i = 0; i < monstersInScene.Length; i++) {
                monstersInScene[i].GetComponent<Animator>().speed = 0f;
            }
        } else {
            GameObject.Find("Water").GetComponent<Animator>().speed = 1f;
            for (int i = 0; i < obstaclesInScene.Length; i++) {
                obstaclesInScene[i].transform.GetChild(0).GetComponent<Animator>().speed = 1f;
            }
            for (int i = 0; i < monstersInScene.Length; i++) {
                monstersInScene[i].GetComponent<Animator>().speed = 1f;
            }
        }
    }

    //Método para dar gameOver no jogo.
    void ChangeGameOver(){
        ambienceAudio.Stop();
        player.SendMessage("SetPaused", this.isGameOver); // Parar o player
        obstacleManager.SendMessage("SetPaused", this.isGameOver); // Parar a contrução de novos obstáculos
		player.GetComponent<Rigidbody2D>().isKinematic = true;
		GameObject.Find("Corners").gameObject.SendMessage("SetPaused", this.isGameOver); // Parar o offset dos corners

        // Parar os obstaculos
        GameObject[] obstaclesInScene = GameObject.FindGameObjectsWithTag("Obstacle");
        for (int i = 0; i < obstaclesInScene.Length; i++) {
            obstaclesInScene[i].SendMessage("SetMove", this.isGameOver);
        }

        // Parar os monstros
        GameObject[] monstersInScene = GameObject.FindGameObjectsWithTag("Monster");
        for (int i = 0; i < monstersInScene.Length; i++) {
            monstersInScene[i].SendMessage("SetMove", this.isGameOver);
        }

        // Quando o jogo acaba, parar todas as animações e mostrar o menu de Game Over.
        if (this.isGameOver) {
            BestScoreGameOverPanel.text = string.Format("  {0}m", GetBestScore());
            ActualScoreGameOverPanel.text = string.Format("  {0}m", (int) this.distance);

            setGameOverTrigger = true;
            GameObject.Find("Water").GetComponent<Animator>().speed = 0f;
            for (int i = 0; i < obstaclesInScene.Length; i++) {
                obstaclesInScene[i].transform.GetChild(0).GetComponent<Animator>().speed = 0f;
                obstaclesInScene[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
            for (int i = 0; i < monstersInScene.Length; i++) {
                monstersInScene[i].GetComponent<Animator>().speed = 0f;
            }
        }
    }

    void SaveBestScore() {
        if (PlayerPrefs.GetInt(Message.BestScore).Equals(null)) {
            PlayerPrefs.SetInt(Message.BestScore, (int) this.distance);
        } else {
            int bestScore = PlayerPrefs.GetInt(Message.BestScore);
            if (bestScore < (int)this.distance) {
                PlayerPrefs.SetInt(Message.BestScore, (int)this.distance);
            }
        }
    }

    int GetBestScore() {
        if (PlayerPrefs.GetInt(Message.BestScore).Equals(null)) {
            return 0;
        } else {
            int bestScore = PlayerPrefs.GetInt(Message.BestScore);
            return bestScore;
        }
    }
}