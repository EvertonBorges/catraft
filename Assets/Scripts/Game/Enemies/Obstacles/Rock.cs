﻿using UnityEngine;
using System.Collections;

public class Rock : Obstacle {

    protected override void move() {
        transform.position += Vector3.up * speed * Time.deltaTime;
    }

}
