﻿using UnityEngine;
using System.Collections;

public class Trunk : Obstacle {

    protected override void move() {
        transform.position += Vector3.up * speed * Time.deltaTime;
    }
}