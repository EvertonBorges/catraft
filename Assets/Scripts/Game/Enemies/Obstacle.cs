﻿using UnityEngine;
using System.Collections;

public abstract class Obstacle : MonoBehaviour {

    public float speed;
    public bool isMove;
	protected Vector2 startPosition;

	// Use this for initialization
	public virtual void Start (){
		startPosition = transform.position;
		this.speed *= GameObject.Find ("GameManager").GetComponent<GameManager> ().speedGame;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y > 6f) {
			Destroy(this.gameObject);
		}

		if (isMove) {
			move();
        }
	}

    protected abstract void move();

    void SetMove(bool isMove) {
        this.isMove = !isMove;
    }

	void OnTriggerStay2D(Collider2D collider) {
		if (collider.CompareTag("Corner")) {
			float cornerPositionX = collider.transform.GetComponent<BoxCollider2D>().transform.position.x;
			float cornerScaleX = collider.transform.GetComponent<BoxCollider2D>().bounds.size.x;
			float playerScaleX = GetComponent<PolygonCollider2D> ().bounds.size.x / 2;

			if (collider.transform.GetComponent<Corner>().isLeft) {
				float newpositionX = (cornerPositionX + cornerScaleX) + playerScaleX;
				transform.position = new Vector3(newpositionX, transform.position.y, transform.position.z);
			}
			else {
				float newpositionX = (cornerPositionX - cornerScaleX) - playerScaleX;
				transform.position = new Vector3(newpositionX, transform.position.y, transform.position.z);
			}
		}
	}
}