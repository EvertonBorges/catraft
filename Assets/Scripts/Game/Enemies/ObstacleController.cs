﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour {

    [Range(0.25f, 0.75f)]
    public float positionXObject1, positionXObject2;

    // Use this for initialization
    void Start () {
        float convertion1 = Camera.main.ViewportToWorldPoint(new Vector3(positionXObject1, 0f, 0f)).x;
        float convertion2 = Camera.main.ViewportToWorldPoint(new Vector3(positionXObject2, 0f, 0f)).x;
        Vector3 positionObject1 = transform.GetChild(0).position;
        Vector3 positionObject2 = transform.GetChild(1).position;
        transform.GetChild(0).position = new Vector3(convertion1, positionObject1.y, positionObject1.z);
        transform.GetChild(1).position = new Vector3(convertion2, positionObject2.y, positionObject2.z);
    }
}
