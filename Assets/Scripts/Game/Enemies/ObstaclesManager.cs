﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObstaclesManager : MonoBehaviour {
    
    public GameObject[] prefabObstacles;

    private Vector2 positionCenter;
    private float pointOfSpawnY;
    private bool isPaused;
    private GameObject lastObject;
    private int level;

	// Use this for initialization
	void Start () {
        positionCenter = new Vector2(0f, -7f);
        level = 0;
        pointOfSpawnY = 1f;
        SortAPrefab();
	}

    void FixedUpdate() {
        if (isPaused) {
            return;
        }

        if (lastObject == null) {
            SortAPrefab();
        }

        int childCount = lastObject.transform.childCount;
        if (childCount > 0) {
            if (lastObject.transform.GetChild(0).position.y > pointOfSpawnY) {
                SortAPrefab();
            } else {
                int newChildCount = lastObject.transform.GetChild(0).childCount;
                if (newChildCount > 0) {
                    if (lastObject.transform.GetChild(0).GetChild(0).position.y > pointOfSpawnY) {
                        SortAPrefab();
                    }
                }
            }
        }

        int thisChildsCount = transform.childCount;
        for (int i = 0; i < thisChildsCount; i++) {
            if (transform.GetChild(i).childCount == 0) {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    void SortAPrefab() {
        float sortedRangeX = Random.Range(0.2f, 0.8f);
        sortedRangeX = Camera.main.ViewportToWorldPoint(new Vector3(sortedRangeX, 0f, 0f)).x;
        Vector2 newPosition = new Vector2(sortedRangeX, positionCenter.y);
        int sortedPrefabIndex;
        if (level < 9) {
            sortedPrefabIndex = Random.Range(0, level);
        } else {
            sortedPrefabIndex = Random.Range(0, prefabObstacles.Length);
        }
        if (sortedPrefabIndex < 9) {
            lastObject = Instantiate(prefabObstacles[sortedPrefabIndex], newPosition, Quaternion.identity) as GameObject;
        } else {
            lastObject = Instantiate(prefabObstacles[sortedPrefabIndex], new Vector2(0f, positionCenter.y), Quaternion.identity) as GameObject;
        }
        lastObject.transform.parent = this.transform;
    }

    void SetPaused(bool isPaused) {
        this.isPaused = isPaused;
    }

    void SetLevel() {
        if (pointOfSpawnY > -2f) {
            level++;
            pointOfSpawnY -= 0.25f;
        }
    }
}