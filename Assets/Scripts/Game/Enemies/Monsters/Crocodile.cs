﻿using UnityEngine;
using System.Collections;

public class Crocodile : Obstacle {

    private bool isLeft;

    public override void Start() {
        base.Start();
        if (Random.Range(0, 2) == 0) {
            isLeft = true;
        } else {
            isLeft = false;
        }
    }

    protected override void move() {
        if (isLeft) {
		    transform.position += Vector3.up * Time.deltaTime * speed + Vector3.left * Time.deltaTime * speed / 2;
        } else {
            transform.position += Vector3.up * Time.deltaTime * speed + Vector3.right * Time.deltaTime * speed / 2;
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Corner")) {
            isLeft = !isLeft;
        }
    }
}