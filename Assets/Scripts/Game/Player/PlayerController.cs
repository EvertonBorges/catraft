﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
    
    [Header("Controller")]
	public float force;

    [Header("Pause Button")]
    public Image pauseButton;

    [Header("Hit Controller")]
    public Image hitPanel;

    // Animations Controllers
    private Animator m_animator;
    private bool setSwimTrigger = false;
    private bool setFearTrigger = false;
    private bool setHitTrigger = false;
    private bool isFear = false;
    private bool isFacingLeft = false;
	private int life;
	private float actualInvencibleTime;
	private float invencibleTime;
	private bool isHitted;
    private bool isPaused;
    private Vector2 lastSpeed;
	private Vector2 lastVelocity;
	private Vector2 actualVelocity;
    private float TimeStoped;

	// Use this for initialization
	void Start () {
		life = 3;
        m_animator = GetComponent<Animator>();
		invencibleTime = 3f;
		isHitted = false;
		actualVelocity = Vector2.zero;
		lastVelocity = Vector2.zero;
	}

    // Update is called once per frame
    void Update() {
		Vector2 actualVelocity = GetComponent<Rigidbody2D> ().velocity;
        if (Mathf.Abs(actualVelocity.x) > 0.5f) {
            TimeStoped = 0f;
        }


        TimeStoped += Time.deltaTime;

		if (this.actualVelocity != actualVelocity) {
			lastVelocity = this.actualVelocity;
			this.actualVelocity = actualVelocity;
		}

        if (isPaused) {
            return;
        }

        if (!m_animator.IsInTransition(0)) {
            UpdateAnimatorParameters();
            CheckHitted();
        }
	}

    void UpdateAnimatorParameters() {
        
        m_animator.SetBool("IsFear", isFear);
        m_animator.SetFloat("TimeStoped", TimeStoped);
        m_animator.SetFloat("Velocity", actualVelocity.x);

        // Swim
        if (setSwimTrigger) {
            m_animator.SetTrigger("Swim");
            setSwimTrigger = false;
        } else {
            m_animator.ResetTrigger("Swim");
        }

        // Fear
        if (setFearTrigger) {
            m_animator.SetTrigger("Fear");
            setFearTrigger = false;
        } else {
            m_animator.ResetTrigger("Fear");
        }

        // Hitted
        if (setHitTrigger) {
            m_animator.SetTrigger("Hit");
            setHitTrigger = false;
        } else {
            m_animator.ResetTrigger("Hit");
        }
    }

	public void moveRight(){
        if (isPaused) {
            return;
        }

        TimeStoped = 0f;
        setSwimTrigger = true;

        GetComponent<Rigidbody2D> ().AddForce(Vector3.right * force);

		if (isFacingLeft) {
			isFacingLeft = false;
			MirrorSprite(-1, 1);
		}
	}

	public void moveLeft(){
        if (isPaused) {
            return;
        }

        TimeStoped = 0f;
        setSwimTrigger = true;

        GetComponent<Rigidbody2D> ().AddForce(Vector3.left * force);

		if (!isFacingLeft) {
			isFacingLeft = true;
			MirrorSprite(-1, 1);
		}
	}

    void CheckHitted() {
        int layerMonster = LayerMask.NameToLayer("Monster");
        int layerObstacle = LayerMask.NameToLayer("Obstacle");

        Collider2D hit2DMonster = IsObstacleNear("Monster", layerMonster);
        Collider2D hit2DObstacle = IsObstacleNear("Obstacle", layerObstacle);
        if (isHitted && (hit2DMonster || hit2DObstacle)) {
            Physics2D.IgnoreLayerCollision(this.gameObject.layer, layerMonster, true);
            Physics2D.IgnoreLayerCollision(this.gameObject.layer, layerObstacle, true);
        }
        if (!isHitted) {
            Physics2D.IgnoreLayerCollision(this.gameObject.layer, layerMonster, false);
            Physics2D.IgnoreLayerCollision(this.gameObject.layer, layerObstacle, false);
            if (hit2DMonster != null && !isFear) {
                setFearTrigger = true;
                isFear = true;
            } else {
                if (hit2DMonster != null && isFear) {
                    setFearTrigger = false;
                } else if (hit2DMonster == null) {
                    isFear = false;
                }
            }
        }

        HittedTime();
    }

	Collider2D IsObstacleNear(string layerName, int layer){
        layer = 1 << layer;
        return Physics2D.OverlapCircle(transform.position, 2f, layer);
	}

    void MirrorSprite(float x, float y) {
		Vector3 localScale = transform.localScale;

        localScale.x *= x;
		localScale.y *= y;

		transform.localScale = localScale;
	}

	void SetForce(int force){
		this.force *= 1.1f;
	}

	void OnTriggerEnter2D(Collider2D collider){
        if (life > 0) {
		    if (collider.CompareTag("Obstacle") || collider.CompareTag("Monster")) {
                hitPanel.GetComponent<Animator>().SetTrigger("Hit");
                life--;
                GameObject.Find("GameManager").GetComponent<GameManager>().SendMessage("ChangeLife", life);
			    isHitted = true;
                setHitTrigger = true;
		    }
        }
	}

	void OnCollisionStay2D(Collision2D collision) {
		if (collision.collider.CompareTag("Corner")) {
			float cornerPositionX = collision.transform.GetComponent<BoxCollider2D>().transform.position.x;
			float cornerScaleX = collision.transform.GetComponent<BoxCollider2D>().bounds.size.x;
			float playerScaleX = GetComponent<CircleCollider2D> ().radius / 2;

            if (collision.transform.GetComponent<Corner>().isLeft) {
                float newpositionX = (cornerPositionX + cornerScaleX) + playerScaleX;
                transform.position = new Vector3(newpositionX, transform.position.y, transform.position.z);
				GetComponent<Rigidbody2D> ().velocity = new Vector2(-lastVelocity.x / 4, lastVelocity.y);
				if (isFacingLeft) {
					MirrorSprite (-1, 1);
					isFacingLeft = false;
				}
            }
            else {
                float newpositionX = (cornerPositionX - cornerScaleX) - playerScaleX;
                transform.position = new Vector3(newpositionX, transform.position.y, transform.position.z);
				GetComponent<Rigidbody2D> ().velocity = new Vector2(-lastVelocity.x / 4, lastVelocity.y);
				if (!isFacingLeft) {
					MirrorSprite (-1, 1);
					isFacingLeft = true;
				}
            }
        }
    }

	void HittedTime() {
		if (isHitted) {
			actualInvencibleTime += Time.deltaTime;
			if (actualInvencibleTime > invencibleTime) {
				isHitted = false;
				actualInvencibleTime = 0f;
			}
		}
	}

    void SetPaused(bool isPaused) {
        this.isPaused = isPaused;
        if (isPaused) {
            lastSpeed = GetComponent<Rigidbody2D>().velocity;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        } else {
            GetComponent<Rigidbody2D>().velocity = lastSpeed;
            lastSpeed = Vector2.zero;
        }
    }
}
